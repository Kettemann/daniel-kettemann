import {Component, Input, ViewChild, Inject} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {MatSidenav} from '@angular/material';
import { WINDOW } from '@ng-toolkit/universal';

interface ROUTE {
  icon?: string;
  route?: string;
  title?: string;
}


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  myWorkRoutes: ROUTE[] = [
    // {
    //   icon: 'assignment',
    //   route: '#about-me',
    //   title: 'Über mich',
    // }, {
    //   icon: 'dashboard',
    //   route: '#experience',
    //   title: 'Berufserfahrung',
    // },{
    //   icon: 'dashboard',
    //   route: '#portfolio',
    //   title: 'Projekte',
    // },{
    //   icon: 'dashboard',
    //   route: '#skills',
    //   title: 'Kenntnisse',
    // },

  ];

  @ViewChild('drawer') public drawer: MatSidenav
  screenWidth: number

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(@Inject(WINDOW) private window: Window, private breakpointObserver: BreakpointObserver) {

    // set screenWidth on page load
    this.screenWidth = window.innerWidth
    window.onresize = () => {
      // set screenWidth on screen size change
      this.screenWidth = window.innerWidth
    }
  }

}
