import {Component, ElementRef, OnInit, ViewChild, Inject} from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
import {ActivatedRoute, Router} from '@angular/router';
import { WINDOW } from '@ng-toolkit/universal';



@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  @ViewChild('portfolio') portfolio: ElementRef;
  distance = NaN;
  hash = "";
  currentHash = "";

  myPortfolio_1: PortfolioItem[] = [
    {
      imgPath: 'assets/sample_code.jpeg',
      title: 'OneStudies',
      description: 'Eine Webseite, die Studierende beim Lernen unterstützt.',
      link: 'https://onestudies.com',
      code_link: undefined
    },
    {
      imgPath: 'assets/color_code.jpeg',
      title: 'Angular App Template',
      description: 'Ein Template für responsive Angular Anwendungen.',
      link: 'http://template.daniel-kettemann.com',
      code_link: 'https://gitlab.com/onestudies/angular-template/'
    },
    {
      imgPath: 'assets/sample_code.jpeg',
      title: 'Camera App',
      description: 'Eine Kamera App zur Übermittlung von Bildern an einen Server, geschrieben in Node.js mithilfe von Socket.js.',
      link: 'https://analytics.onestudies.com',
      code_link: undefined
    },
  ];

  myPortfolio_2: PortfolioItem[] = [
    {
      imgPath: 'assets/color_code.jpeg',
      title: 'Sample Webshop',
      description: 'Ein Prestashop System, umgearbeitet von Apache auf NGINX Rewrite Rules.',
      link: 'http://shop.daniel-kettemann.com',
      code_link: undefined
    },

    {
      imgPath: 'assets/sample_code.jpeg',
      title: 'Chat App',
      description: 'Eine minimalistische Chat App, geschrieben in Node.js mithilfe von Socket.js.',
      link: 'http://52.29.244.136',
      code_link: undefined
    },

  ];

  presentWork: PortfolioItem[] = [
    {
      imgPath: 'assets/sample_code.jpeg',
      title: 'Growfessional',
      description: 'Ein Webshop, über den LED Beleuchtung an Gärtnereien verkauft wird.',
      link: 'https://www.growfessional.de',
      code_link: undefined
    }
  ];

  constructor(@Inject(WINDOW) private window: Window,
              private scrollDispatcher: ScrollDispatcher) {
    this.scrollDispatcher.scrolled().subscribe(x => {
      if(document.documentElement.scrollTop==0){
        this.currentHash = ''
        this.navigateToSomeLocation('')
      }
      // var top = window.pageYOffset;
      var elem = document.getElementById('portfolio') as HTMLElement;
      this.distance = elem.getBoundingClientRect().top;
      this.hash = elem.id;

      if (this.distance < 30 && this.distance > -30 && this.currentHash != this.hash) {
        console.log(this.hash);
        this.currentHash = this.hash;
        this.navigateToSomeLocation(this.hash);
      }
    });
  }

  ngOnInit() {
    this.window.addEventListener("hashchange", function () {
      this.window.scrollTo(this.window.scrollX, this.window.scrollY - 66);
    });
  }

  navigateToSomeLocation(location: string){
    console.log('navigate to location ' + location);
    this.window.location.href = "#" + location;
  }

}

class PortfolioItem{
  imgPath: string;
  title: string;
  description: string;
  link: string;
  code_link: string;
}
